#pragma once

// UnityDLLExample.h : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include <stdlib.h>
#include <math.h>

//model structure
typedef struct {
	double * x; //computational scratch space
	double * x2; //computational scratch space
	double * store; //computational scratch space
	double * k1; //computational scratch space
	double * k2;  //computational scratch space
	double * k3;  //computational scratch space
	double * k4;  //computational scratch space
	double * mass;  //masses
	double shield; //shield radius 
	/*double softening_factor;*/
	double G;  //graviational constant
	double m;  
	int n;  //number of points
	int t;  //time of model
	int abmCounter; //restart counter for ABM integration 
} Model;

extern "C" __declspec(dllexport) void * createModel(int n);
extern "C" __declspec(dllexport) void stepModelRK2(void * voo, double dt);
extern "C" __declspec(dllexport) void stepModelRK4(void * voo, double dt);
//Added LeapFrog Method attempt 
//extern "C" __declspec(dllexport) void stepNbodyModelLeapfrog(void * voo, double dt);

extern "C" __declspec(dllexport) void stepNbodyModelABM(void * voo, double tStep);

extern "C" __declspec(dllexport) void stepModelEuler(void * voo, double dt);
extern "C" __declspec(dllexport) double getX(void * voo, int i);
extern "C" __declspec(dllexport) double * getXArray(void * voo);
extern "C" __declspec(dllexport) void destroyModel(void * voo);
extern "C" __declspec(dllexport) void ratesOfChange(Model * foo, double* x, double* xdot);
extern "C" __declspec(dllexport) double randRange(double low, double high);
extern "C" __declspec(dllexport) void setX(void * voo, double value, int i);
extern "C" __declspec(dllexport) void setXArray(void * voo, double * value);


extern "C" _declspec(dllexport) void setTStep(void * voo, double tstep);
/*extern "C" __declspec(dllexport) int setSofteningNbodyModel(void * voo);*/

//Setting and changing G
extern "C" _declspec(dllexport) double getGFromSI(double mass, double length, double time);
extern "C" __declspec(dllexport) void setG(void * voo, double G);
